**Neanex REST API**

This is the REST API for the Neanex application, which provides functionality related to buildings, rooms, sensors, and sensor readings.


## Prerequisites

- Java 8 or higher
- Spring Boot
- Neo4j Database

## Getting started
- Clone the repository.
- Build the project using Maven: mvn clean install.
- Run the application: mvn spring-boot:run.
- The application will be accessible at http://localhost:8080.

## Properties
In application.properties adjust following properties
spring.neo4j.uri=
spring.neo4j.authentication.username=
spring.neo4j.authentication.password=

## Sensor Reading App

This is a RESTful API application for managing sensor readings in rooms and buildings.
Technologies Used

    Java
    Spring Boot
    Spring Web
    Spring Data
    Maven

## ENDPOINTS
1. `GET /rooms/getAverageTemperature`

Calculates the average temperature of the specified type (temperature or humidity) in the given rooms.

Parameters:

    roomIds (required): List of room IDs.
    startDate (optional): Start date for filtering the sensor readings.
    endDate (optional): End date for filtering the sensor readings.

Returns the average reading value.

2. `GET /rooms/getAverageHumidity`

Calculates the average humidity of the specified type (temperature or humidity) in the given rooms.

Parameters:

    roomIds (required): List of room IDs.
    startDate (optional): Start date for filtering the sensor readings.
    endDate (optional): End date for filtering the sensor readings.


3. `GET /rooms/getReadingValueInRooms`

Retrieves the reading value in rooms based on the provided parameters.

Parameters:

    roomIds (required): List of room IDs to consider.
    readingType (required): Type of reading (temperature or humidity).
    valueType (required): Type of value to retrieve (max or min).
    startDate (optional): Start date for filtering the sensor readings.
    endDate (optional): End date for filtering the sensor readings.

Returns the reading value in rooms based on the specified reading type and value type, or null if the reading type or value type is invalid.

4. `GET /rooms/getAllRoomIds`

Retrieves a list of all room IDs.

Returns a list of room IDs.

5. `GET /rooms/getRoomWithSensorsAndReadingsById`

Retrieves a room with its associated sensors and readings based on the provided room ID.

Parameters:

    roomId (required): Room ID.

Returns the room with sensors and readings.

6. `GET /buildings/getAllBuildings`

Retrieves a list of all buildings.

Returns a list of buildings.
7. `GET /buildings/getBuildingsSensorReadingSummary`

Retrieves the sensor reading summary for a list of buildings based on the provided parameters.

Parameters:

    buildingIds (required): List of building IDs.
    startDate (optional): Start date for filtering the sensor readings.
    endDate (optional): End date for filtering the sensor readings.

Returns the sensor reading summary for the buildings.
8. `GET /buildings/getAverageTemperature`

Calculates the average temperature of the specified type (temperature or humidity) in the given buildings.

Parameters:

    buildingIds (required): List of building IDs.
    startDate (optional): Start date for filtering the sensor readings.
    endDate (optional): End date for filtering the sensor readings.

Returns the average reading value.
9. `GET /buildings/getAverageHumidity`

Calculates the average humidity of the specified type (temperature or humidity) in the given buildings.

Parameters:

    buildingIds (required): List of building IDs.
    startDate (optional): Start date for filtering the sensor readings.
    endDate (optional): End date for filtering the sensor readings.

Returns the average reading value.

10. `POST /sensors/createSensorReading`

Creates a new sensor reading.

Parameters:

    sensorId (required): ID of the sensor.
    temperatureValue (required): Temperature value.
    humidityValue (required): Humidity value.

Returns the created sensor reading.
11. `POST /sensors/createFakeSensorReading`

Creates a new fake sensor reading with random temperature and humidity values.

Parameters:

    sensorId (required): ID of the sensor.

Returns the created sensor reading.
12. `GET /sensors/getAllSensors`

Retrieves a list of all sensors.

Returns a list of sensors.
13. `GET /sensors/getSensor`

Retrieves a sensor with its associated readings based on the provided sensor ID.

Parameters:

    sensorId (required): Sensor ID.

Returns the sensor with readings.


## Configuring database example
    // Building 1
    CREATE (b1:Building {name: 'Building 1'})
    FOREACH (i IN range(1, 2) |
    CREATE (r:Room {name: 'Room ' + toString(i) + ' in Building 1'})
    FOREACH (j IN range(1, 2) |
    CREATE (s:Sensor {type: 'Multitype', roomId: id(r)}) // Set the roomId property
    CREATE (sr:SensorReading {
    timestamp: datetime(),
    temperatureValue: round(rand() * 5 + 15 + i * j),
    humidityValue: round(rand() * 10 + 40 + i * j)
    })
    MERGE (b1)-[:HAS_ROOM]->(r)
    MERGE (r)-[:HAS_SENSOR]->(s)
    MERGE (s)-[:HAS_READING]->(sr)
    )
    )
