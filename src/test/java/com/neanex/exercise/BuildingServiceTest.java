import com.neanex.exercise.model.Building;
import com.neanex.exercise.model.SensorReadingSummary;
import com.neanex.exercise.repository.BuildingRepository;
import com.neanex.exercise.service.BuildingService;
import com.neanex.exercise.service.RoomService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class BuildingServiceTest {
    @Mock
    private BuildingRepository buildingRepository;

    @Mock
    private RoomService roomService;

    private BuildingService buildingService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        buildingService = new BuildingService(buildingRepository, roomService);
    }


    @Test
    void testGetBuildingSensorReadingSummary() {
        // Mocking the repository method to return a list of room IDs
        List<Long> buildingIds = List.of(1L, 2L);
        List<Long> roomIds = List.of(100L, 200L);
        when(buildingRepository.findRoomIdsByBuildingIds(buildingIds)).thenReturn(roomIds);

        // Mocking the room service method to return a sensor reading summary
        SensorReadingSummary summary = new SensorReadingSummary();
        summary.setMaxTemperature(25.5);
        summary.setMinHumidity(50.0);
        when(roomService.getSensorReadingSummary(roomIds, null, null)).thenReturn(summary);

        // Calling the service method
        SensorReadingSummary result = buildingService.getBuildingSensorReadingSummary(buildingIds, null, null);

        // Verifying the repository and service methods were called and the result is as expected
        verify(buildingRepository, times(1)).findRoomIdsByBuildingIds(buildingIds);
        verify(roomService, times(1)).getSensorReadingSummary(roomIds, null, null);
        assertEquals(25.5, result.getMaxTemperature());
        assertEquals(50.0, result.getMinHumidity());
    }

    // Add additional test methods for other service methods

}
