package com.neanex.exercise;

import com.neanex.exercise.exception.ResourceNotFoundException;
import com.neanex.exercise.model.Room;
import com.neanex.exercise.model.Sensor;
import com.neanex.exercise.model.SensorReadingSummary;
import com.neanex.exercise.repository.RoomRepository;
import com.neanex.exercise.service.RoomService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class RoomServiceTest {

    @Mock
    private RoomRepository roomRepository;

    @InjectMocks
    private RoomService roomService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAverageTemperature() {
        List<Long> roomIds = Arrays.asList(1L, 2L, 3L);
        String startDate = "2022-01-01";
        String endDate = "2022-12-31";
        double expectedAverageTemperature = 25.0;

        when(roomRepository.getAverageTemperature(roomIds, startDate, endDate)).thenReturn(expectedAverageTemperature);

        double actualAverageTemperature = roomService.getAverageTemperature(roomIds, startDate, endDate);

        Assertions.assertEquals(expectedAverageTemperature, actualAverageTemperature);
        verify(roomRepository, times(1)).getAverageTemperature(roomIds, startDate, endDate);
    }

    @Test
    void testGetAverageHumidity() {
        List<Long> roomIds = Arrays.asList(1L, 2L, 3L);
        String startDate = "2022-01-01";
        String endDate = "2022-12-31";
        double expectedAverageHumidity = 50.0;

        when(roomRepository.getAverageHumidity(roomIds, startDate, endDate)).thenReturn(expectedAverageHumidity);

        double actualAverageHumidity = roomService.getAverageHumidity(roomIds, startDate, endDate);

        Assertions.assertEquals(expectedAverageHumidity, actualAverageHumidity);
        verify(roomRepository, times(1)).getAverageHumidity(roomIds, startDate, endDate);
    }

    @Test
    void testGetSensorReadingSummary() {
        List<Long> roomIds = Arrays.asList(1L, 2L, 3L);
        String startTime = "2022-01-01T00:00:00";
        String endTime = "2022-12-31T23:59:59";

        SensorReadingSummary expectedSummary = new SensorReadingSummary();
        expectedSummary.setMinTemperature(20.0);
        expectedSummary.setMaxTemperature(30.0);
        expectedSummary.setMinHumidity(40.0);
        expectedSummary.setMaxHumidity(60.0);

        Map<String, Double> result = Map.of(
                "minTemperature", expectedSummary.getMinTemperature(),
                "maxTemperature", expectedSummary.getMaxTemperature(),
                "minHumidity", expectedSummary.getMinHumidity(),
                "maxHumidity", expectedSummary.getMaxHumidity()
        );
        Iterable<Map<String, Double>> results = Arrays.asList(result);

        when(roomRepository.findSensorReadingSummary(roomIds, startTime, endTime)).thenReturn(results);

        SensorReadingSummary actualSummary = roomService.getSensorReadingSummary(roomIds, startTime, endTime);

        Assertions.assertEquals(expectedSummary.getMinTemperature(), actualSummary.getMinTemperature());
        Assertions.assertEquals(expectedSummary.getMaxTemperature(), actualSummary.getMaxTemperature());
        Assertions.assertEquals(expectedSummary.getMinHumidity(), actualSummary.getMinHumidity());
        Assertions.assertEquals(expectedSummary.getMaxHumidity(), actualSummary.getMaxHumidity());

        verify(roomRepository, times(1)).findSensorReadingSummary(roomIds, startTime, endTime);
    }

    @Test
    void testGetSensorReadingSummaryWithNoResults() {
        List<Long> roomIds = Arrays.asList(1L, 2L, 3L);
        String startTime = "2022-01-01T00:00:00";
        String endTime = "2022-12-31T23:59:59";

        Iterable<Map<String, Double>> results = new ArrayList<>();

        when(roomRepository.findSensorReadingSummary(roomIds, startTime, endTime)).thenReturn(results);

        SensorReadingSummary actualSummary = roomService.getSensorReadingSummary(roomIds, startTime, endTime);

        Assertions.assertNull(actualSummary.getMinTemperature());
        Assertions.assertNull(actualSummary.getMaxTemperature());
        Assertions.assertNull(actualSummary.getMinHumidity());
        Assertions.assertNull(actualSummary.getMaxHumidity());

        verify(roomRepository, times(1)).findSensorReadingSummary(roomIds, startTime, endTime);
    }

    @Test
    void testGetAllRoomIds() {
        List<Long> expectedRoomIds = Arrays.asList(1L, 2L, 3L);

        when(roomRepository.findAllRoomIds()).thenReturn(expectedRoomIds);

        List<Long> actualRoomIds = roomService.getAllRoomIds();

        Assertions.assertEquals(expectedRoomIds, actualRoomIds);
        verify(roomRepository, times(1)).findAllRoomIds();
    }

    @Test
    void testGetRoomWithSensorsAndReadingsById() {
        Long roomId = 1L;
        Room expectedRoom = new Room();
        expectedRoom.setId(roomId);
        Sensor sensor1 = new Sensor();
        Sensor sensor2 = new Sensor();
        List<Sensor> expectedSensors = Arrays.asList(sensor1, sensor2);
        expectedRoom.setSensors(expectedSensors);

        when(roomRepository.findById(roomId)).thenReturn(java.util.Optional.of(expectedRoom));

        Room actualRoom = roomService.getRoomWithSensorsAndReadingsById(roomId);

        Assertions.assertEquals(expectedRoom, actualRoom);
        Assertions.assertEquals(expectedSensors, actualRoom.getSensors());
        verify(roomRepository, times(1)).findById(roomId);
    }

    @Test
    void testGetRoomWithSensorsAndReadingsByIdWithInvalidId() {
        Long roomId = 1L;

        when(roomRepository.findById(roomId)).thenReturn(java.util.Optional.empty());

        Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            roomService.getRoomWithSensorsAndReadingsById(roomId);
        });

        verify(roomRepository, times(1)).findById(roomId);
    }
}
