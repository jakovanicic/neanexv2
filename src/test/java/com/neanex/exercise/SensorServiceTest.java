package com.neanex.exercise;

import com.neanex.exercise.exception.ResourceNotFoundException;
import com.neanex.exercise.model.Sensor;
import com.neanex.exercise.model.SensorReading;
import com.neanex.exercise.repository.SensorReadingRepository;
import com.neanex.exercise.repository.SensorRepository;
import com.neanex.exercise.service.SensorService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class SensorServiceTest {

    @Mock
    private SensorRepository sensorRepository;

    @Mock
    private SensorReadingRepository sensorReadingRepository;

    @InjectMocks
    private SensorService sensorService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createSensorReading_ValidSensorId_ReturnsSensorReading() {
        Long sensorId = 1L;
        Double temperatureValue = 20.0;
        Double humidityValue = 50.0;

        Sensor sensor = new Sensor();
        sensor.setId(sensorId);

        when(sensorRepository.findById(sensorId)).thenReturn(Optional.of(sensor));
        when(sensorReadingRepository.save(any(SensorReading.class))).thenAnswer(invocation -> invocation.getArgument(0));

        SensorReading result = sensorService.createSensorReading(sensorId, temperatureValue, humidityValue);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(temperatureValue, result.getTemperatureValue());
        Assertions.assertEquals(humidityValue, result.getHumidityValue());
        Assertions.assertEquals(sensor, result.getSensor());
        Assertions.assertNotNull(sensor.getSensorReadings());
        Assertions.assertTrue(sensor.getSensorReadings().contains(result));
    }

    @Test
    void createSensorReading_InvalidSensorId_ThrowsResourceNotFoundException() {
        Long sensorId = 1L;
        Double temperatureValue = 20.0;
        Double humidityValue = 50.0;

        when(sensorRepository.findById(sensorId)).thenReturn(Optional.empty());

        Assertions.assertThrows(ResourceNotFoundException.class,
                () -> sensorService.createSensorReading(sensorId, temperatureValue, humidityValue));
    }

    @Test
    void getAllSensors_ReturnsListOfSensors() {
        List<Sensor> expectedSensors = new ArrayList<>();
        expectedSensors.add(new Sensor());
        expectedSensors.add(new Sensor());

        when(sensorRepository.findAll()).thenReturn(expectedSensors);

        List<Sensor> result = sensorService.getAllSensors();

        Assertions.assertEquals(expectedSensors, result);
    }

    @Test
    void getSensorById_ValidSensorId_ReturnsSensor() {
        Long sensorId = 1L;
        Sensor expectedSensor = new Sensor();
        expectedSensor.setId(sensorId);

        when(sensorRepository.findById(sensorId)).thenReturn(Optional.of(expectedSensor));

        Sensor result = sensorService.getSensorById(sensorId);

        Assertions.assertEquals(expectedSensor, result);
    }

    @Test
    void getSensorById_InvalidSensorId_ThrowsResourceNotFoundException() {
        Long sensorId = 1L;

        when(sensorRepository.findById(sensorId)).thenReturn(Optional.empty());

        Assertions.assertThrows(ResourceNotFoundException.class, () -> sensorService.getSensorById(sensorId));
    }

    @Test
    void getSensorWithReadingsById_ValidSensorId_ReturnsSensorWithReadings() {
        Long sensorId = 1L;
        Sensor expectedSensor = new Sensor();
        expectedSensor.setId(sensorId);
        expectedSensor.setSensorReadings(new ArrayList<>());

        when(sensorRepository.findById(sensorId)).thenReturn(Optional.of(expectedSensor));

        Sensor result = sensorService.getSensorWithReadingsById(sensorId);

        Assertions.assertEquals(expectedSensor, result);
    }

    @Test
    void getSensorWithReadingsById_InvalidSensorId_ThrowsResourceNotFoundException() {
        Long sensorId = 1L;

        when(sensorRepository.findById(sensorId)).thenReturn(Optional.empty());

        Assertions.assertThrows(ResourceNotFoundException.class,
                () -> sensorService.getSensorWithReadingsById(sensorId));
    }
}
