package com.neanex.exercise.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.springframework.data.neo4j.core.schema.*;

import java.util.List;

@Node
@Data
public class Room {
    @Id
    @GeneratedValue
    private Long id;
    private String name;

    @JsonManagedReference
    @Relationship(type = "HAS_SENSOR", direction = Relationship.Direction.OUTGOING)
    private List<Sensor> sensors;
}

