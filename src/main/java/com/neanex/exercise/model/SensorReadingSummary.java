package com.neanex.exercise.model;

import lombok.Data;

@Data
public class SensorReadingSummary {

    private Double minTemperature;
    private Double maxTemperature;
    private Double maxHumidity;
    private Double minHumidity;

}

