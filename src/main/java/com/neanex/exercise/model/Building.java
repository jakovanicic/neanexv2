package com.neanex.exercise.model;

import lombok.Data;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;

import java.util.List;

@Node
@Data
public class Building {
    @Id
    @GeneratedValue
    private Long id;
    private String name;

    @Relationship(type = "HAS_ROOM", direction = Relationship.Direction.OUTGOING)
    private List<Room> rooms;


}
