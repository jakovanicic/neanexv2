package com.neanex.exercise.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;

import java.util.List;


@Node
@Data
public class Sensor {
    @Id
    @GeneratedValue
    private Long id;
    private String type;

    @JsonIgnoreProperties("sensor")
    @Relationship(type = "HAS_READING", direction = Relationship.Direction.OUTGOING)
    private List<SensorReading> sensorReadings;


    @Relationship(type = "IN_ROOM", direction = Relationship.Direction.INCOMING)
    private Room room;


    @Override
    public String toString() {
        return "Sensor{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }
}