package com.neanex.exercise.model;

import lombok.Data;

import java.util.List;

@Data
public class AverageTemperatureRequest {

        private List<Long> roomIds;
        private String startDate;
        private String endDate;
}
