package com.neanex.exercise.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Relationship;

import java.time.OffsetDateTime;

@Data
@Node
public class SensorReading {
    @Id
    @GeneratedValue
    private Long id;
    private OffsetDateTime timestamp;
    private Double temperatureValue;
    private Double humidityValue;

    @JsonIgnoreProperties("sensorReading")
    @Relationship(type = "HAS_READING", direction = Relationship.Direction.INCOMING)
    private Sensor sensor;
}
