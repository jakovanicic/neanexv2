package com.neanex.exercise.service;

import com.neanex.exercise.exception.ResourceNotFoundException;
import com.neanex.exercise.model.Room;
import com.neanex.exercise.model.Sensor;
import com.neanex.exercise.model.SensorReading;
import com.neanex.exercise.model.SensorReadingSummary;
import com.neanex.exercise.repository.RoomRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class RoomService {
    private final Logger logger = LoggerFactory.getLogger(RoomService.class);

    private final RoomRepository roomRepository;

    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    public Double getAverageTemperature(List<Long> buildingIds, String startDate, String endDate) {
        logger.info("Calculating average temperature for buildings: {}", buildingIds);
        Double averageTemperature = roomRepository.getAverageTemperature(buildingIds, startDate, endDate);
        logger.info("Average temperature for buildings {}: {}", buildingIds, averageTemperature);
        return averageTemperature;
    }

    public Double getAverageHumidity(List<Long> roomIds, String startDate, String endDate) {
        logger.info("Calculating average humidity in rooms: {}", roomIds);
        Double averageHumidity = 0.0;
        averageHumidity = roomRepository.getAverageHumidity(roomIds,startDate,endDate);

        return averageHumidity;
    }




        public SensorReadingSummary getSensorReadingSummary(List<Long> roomIds, String startTime, String endTime) {
        logger.info("Fetching sensor reading summary for rooms: {}, startTime: {}, endTime: {}", roomIds, startTime, endTime);

        Iterable<Map<String, Double>> results = roomRepository.findSensorReadingSummary(roomIds, startTime, endTime);
        SensorReadingSummary summary = new SensorReadingSummary();

        if (results != null) {
            for (Map<String, Double> result : results) {
                summary.setMinTemperature(result.get("minTemperature"));
                summary.setMaxTemperature(result.get("maxTemperature"));
                summary.setMinHumidity(result.get("minHumidity"));
                summary.setMaxHumidity(result.get("maxHumidity"));
                break;
            }
        }

        logger.info("Sensor reading summary fetched: {}", summary);

        return summary;
    }

    public List<Long> getAllRoomIds() {
        logger.info("Fetching all room IDs");
        List<Long> roomIds = roomRepository.findAllRoomIds();
        logger.info("Fetched all room IDs: {}", roomIds);
        return roomIds;
    }

    public Room getRoomWithSensorsAndReadingsById(Long roomId) {
        logger.info("Fetching room with ID: {}", roomId);
        Room room = roomRepository.findById(roomId)
                .orElseThrow(() -> new ResourceNotFoundException("Room not found with ID: " + roomId));
        logger.info("Fetched room: {}", room);

        List<Sensor> sensors = room.getSensors();
        logger.info("Fetched sensors for room with ID {}: {}", roomId, sensors);

        for (Sensor sensor : sensors) {
            List<SensorReading> sensorReadings = sensor.getSensorReadings();
            logger.info("Fetched sensor readings for sensor with ID {}: {}", sensor.getId(), sensorReadings);
        }

        return room;
    }

}
