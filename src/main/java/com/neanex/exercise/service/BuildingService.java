package com.neanex.exercise.service;

import com.neanex.exercise.model.Building;
import com.neanex.exercise.model.SensorReadingSummary;
import com.neanex.exercise.repository.BuildingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
public class BuildingService {
    private final BuildingRepository buildingRepository;

    private RoomService roomService;
    private final Logger logger = LoggerFactory.getLogger(BuildingService.class);

    public BuildingService(BuildingRepository buildingRepository, RoomService roomService) {
        this.buildingRepository = buildingRepository;
        this.roomService = roomService;
    }

    public List<Building> findAllBuildings() {
        logger.info("Retrieving all buildings");
        return buildingRepository.findAll();
    }
    /**
     * Retrieves the sensor reading summary for a building based on the provided parameters.
     *
     * @param buildingIds the IDs of the buildings
     * @param startTime  (optional) the start time for filtering the sensor readings
     * @param endTime    (optional) the end time for filtering the sensor readings
     * @return the sensor reading summary for the building
     */
    public SensorReadingSummary getBuildingSensorReadingSummary(List<Long> buildingIds,String startTime,String endTime) {
        logger.info("Retrieving all Sensor Reading Summary for building + {}",buildingIds);
        List<Long> roomIds = buildingRepository.findRoomIdsByBuildingIds(buildingIds);
        return roomService.getSensorReadingSummary(roomIds, startTime, endTime);
    }


    public Double getAverageTemperature(List<Long> buildingIds, String startDate, String endDate) {
        List<Long> roomIds = buildingRepository.findRoomIdsByBuildingIds(buildingIds);
        return roomService.getAverageTemperature(roomIds,startDate,endDate);
    }

    public Double getAverageHumidity(List<Long> buildingIds, String startDate, String endDate) {
        List<Long> roomIds = buildingRepository.findRoomIdsByBuildingIds(buildingIds);
        return roomService.getAverageHumidity(roomIds,startDate,endDate);
    }
}
