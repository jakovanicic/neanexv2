package com.neanex.exercise.service;

import com.neanex.exercise.exception.ResourceNotFoundException;
import com.neanex.exercise.model.Sensor;
import com.neanex.exercise.model.SensorReading;
import com.neanex.exercise.repository.SensorReadingRepository;
import com.neanex.exercise.repository.SensorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

@Service
public class SensorService {
    private final Logger logger = LoggerFactory.getLogger(SensorService.class);

    private final SensorRepository sensorRepository;

    private final SensorReadingRepository sensorReadingRepository;
    @Autowired
    public SensorService(SensorRepository sensorRepository, SensorReadingRepository sensorReadingRepository, SensorReadingRepository sensorReadingRepository1) {
        this.sensorRepository = sensorRepository;
        this.sensorReadingRepository = sensorReadingRepository1;
    }

    public SensorReading createSensorReading(Long sensorId, Double temperatureValue, Double humidityValue) {
        Sensor sensor = sensorRepository.findById(sensorId)
                .orElseThrow(() -> new ResourceNotFoundException("Sensor not found with ID: " + sensorId));

        SensorReading sensorReading = new SensorReading();
        sensorReading.setTimestamp(OffsetDateTime.now(ZoneOffset.UTC));
        sensorReading.setTemperatureValue(temperatureValue);
        sensorReading.setHumidityValue(humidityValue);


        List<SensorReading> readings = sensor.getSensorReadings();

        if(readings == null) {
            readings = new ArrayList<>();
            sensor.setSensorReadings(readings);
        }
        readings.add(sensorReading);
        sensorReading.setSensor(sensor);

        sensorReadingRepository.save(sensorReading);
        sensorRepository.save(sensor);

        return sensorReading;
    }

    public List<Sensor> getAllSensors() {
        return sensorRepository.findAll();
    }

    public Sensor getSensorById(Long sensorId) {
        return sensorRepository.findById(sensorId)
                .orElseThrow(() -> new ResourceNotFoundException("Sensor not found with ID: " + sensorId));
    }

    public Sensor getSensorWithReadingsById(Long sensorId) {
        logger.info("Fetching sensor with ID: {}", sensorId);
        Sensor sensor = sensorRepository.findById(sensorId)
                .orElseThrow(() -> new ResourceNotFoundException("Sensor not found with ID: " + sensorId));
        logger.info("Fetched sensor: {}", sensor);

        List<SensorReading> sensorReadings = sensor.getSensorReadings();
        logger.info("Fetched sensor readings for sensor with ID {}: {}", sensorId, sensorReadings);

        return sensor;
    }
}
