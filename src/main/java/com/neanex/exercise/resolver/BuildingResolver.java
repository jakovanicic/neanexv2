package com.neanex.exercise.resolver;

import com.neanex.exercise.model.Building;

import java.util.List;

public interface BuildingResolver {
    Double buildings(List<Long> buildingIds, String startDate, String endDate);
}
