package com.neanex.exercise.resolver;

import java.util.List;

public interface TemperatureResolver {
    Double getAverageTemperature(List<Long> buildingIds, String startDate, String endDate);
}
