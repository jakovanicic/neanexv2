package com.neanex.exercise.resolver;

import com.neanex.exercise.model.Building;
import com.neanex.exercise.service.RoomService;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BuildingResolverImpl implements BuildingResolver {

    private final RoomService roomService;

    @Autowired
    public BuildingResolverImpl(RoomService roomService) {
        this.roomService = roomService;
    }

    @Override
    public Double buildings(List<Long> buildingIds, String startDate, String endDate) {
        return roomService.getAverageTemperature(buildingIds, startDate, endDate);
    }

    // Updated resolver method with DataFetchingEnvironment parameter
    public Double buildings(DataFetchingEnvironment environment) {
        List<Long> buildingIds = environment.getArgument("buildingIds");
        String startDate = environment.getArgument("startDate");
        String endDate = environment.getArgument("endDate");
        return roomService.getAverageTemperature(buildingIds, startDate, endDate);
    }
}
