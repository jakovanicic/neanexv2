package com.neanex.exercise.resolver;

import com.neanex.exercise.repository.RoomRepository;
import com.neanex.exercise.service.RoomService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TemperatureResolverImpl implements TemperatureResolver {

    private final Logger logger = LoggerFactory.getLogger(TemperatureResolverImpl.class);

    private final RoomRepository roomRepository;

    @Autowired
    public TemperatureResolverImpl(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Override
    public Double getAverageTemperature(List<Long> buildingIds, String startDate, String endDate) {
        logger.info("Calculating average temperature for buildings: {}", buildingIds);
        Double averageTemperature = roomRepository.getAverageTemperature(buildingIds, startDate, endDate);
        logger.info("Average temperature for buildings {}: {}", buildingIds, averageTemperature);
        return averageTemperature;
    }
}
