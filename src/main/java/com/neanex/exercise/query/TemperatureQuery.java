package com.neanex.exercise.query;

import com.neanex.exercise.resolver.TemperatureResolver;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TemperatureQuery implements GraphQLQueryResolver {

    private final TemperatureResolver temperatureResolver;

    @Autowired
    public TemperatureQuery(TemperatureResolver temperatureResolver) {
        this.temperatureResolver = temperatureResolver;
    }

    public Double getAverageTemperature(List<Long> buildingIds, String startDate, String endDate) {
        return temperatureResolver.getAverageTemperature(buildingIds, startDate, endDate);
    }
}

