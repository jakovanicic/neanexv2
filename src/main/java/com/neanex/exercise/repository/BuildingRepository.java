package com.neanex.exercise.repository;

import com.neanex.exercise.model.Building;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface BuildingRepository extends Neo4jRepository<Building, Long> {

    @Query("MATCH (b:Building)-[:HAS_ROOM]->(r:Room) WHERE ID(b) IN $buildingIds RETURN ID(r) AS roomId")
    List<Long> findRoomIdsByBuildingIds(@Param("buildingIds") List<Long> buildingIds);


}


