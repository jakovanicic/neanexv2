package com.neanex.exercise.repository;

import com.neanex.exercise.model.Sensor;
import org.springframework.data.neo4j.repository.Neo4jRepository;


public interface SensorRepository extends Neo4jRepository<Sensor, Long> {

}