package com.neanex.exercise.repository;

import com.neanex.exercise.model.SensorReading;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface SensorReadingRepository extends Neo4jRepository<SensorReading, Long> {
}
