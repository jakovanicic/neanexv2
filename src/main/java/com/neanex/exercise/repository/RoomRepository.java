package com.neanex.exercise.repository;

import com.neanex.exercise.model.Room;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface RoomRepository extends Neo4jRepository<Room, Long> {
    List<Room> findByIdIn(List<Long> ids);


    @Query("MATCH (r:Room)-[:HAS_SENSOR]->(s:Sensor)-[:HAS_READING]->(sr:SensorReading) " +
            "WHERE ID(r) IN $roomIds " +
            "AND ($startTime IS NULL OR datetime($startTime) <= sr.timestamp) " +
            "AND ($endTime IS NULL OR sr.timestamp <= datetime($endTime)) " +
            "RETURN avg(sr.temperatureValue) AS averageTemperature")
    Double getAverageTemperature(@Param("roomIds") List<Long> roomIds,
                                 @Param("startTime") String startTime,
                                 @Param("endTime") String endTime);

    @Query("MATCH (r:Room)-[:HAS_SENSOR]->(s:Sensor)-[:HAS_READING]->(sr:SensorReading) " +
            "WHERE ID(r) IN $roomIds " +
            "AND ($startTime IS NULL OR datetime($startTime) <= sr.timestamp) " +
            "AND ($endTime IS NULL OR sr.timestamp <= datetime($endTime)) " +
            "RETURN avg(sr.humidityValue) AS averageHumidity")
    Double getAverageHumidity(@Param("roomIds") List<Long> roomIds,
                              @Param("startTime") String startTime,
                              @Param("endTime") String endTime);

    @Query("MATCH (r:Room)-[:HAS_SENSOR]->(s:Sensor)-[:HAS_READING]->(sr:SensorReading) " +
            "WHERE ID(r) IN $roomIds " +
            "AND ($startTime IS NULL OR sr.timestamp >= datetime($startTime)) " +
            "AND ($endTime IS NULL OR sr.timestamp <= datetime($endTime)) " +
            "WITH MIN(sr.temperatureValue) AS minTemperature, MAX(sr.temperatureValue) AS maxTemperature, " +
            "MIN(sr.humidityValue) AS minHumidity, MAX(sr.humidityValue) AS maxHumidity " +
            "RETURN { minTemperature: minTemperature, maxTemperature: maxTemperature, minHumidity: minHumidity, maxHumidity: maxHumidity } AS readingSummary")
    Iterable<Map<String,Double>> findSensorReadingSummary(@Param("roomIds") List<Long> roomIds, @Param("startTime") String startTime, @Param("endTime") String endTime);
    
    @Query("MATCH (r:Room) RETURN ID(r) AS roomId")
    List<Long> findAllRoomIds();

}
