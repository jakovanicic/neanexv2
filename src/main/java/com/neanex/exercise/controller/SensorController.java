package com.neanex.exercise.controller;

import com.neanex.exercise.model.Sensor;
import com.neanex.exercise.model.SensorReading;
import com.neanex.exercise.service.SensorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@RequestMapping("/sensors")
public class SensorController {

    private final SensorService sensorService;

    @Autowired
    public SensorController(SensorService sensorService) {
        this.sensorService = sensorService;

    }

    @PostMapping("/createSensorReading")
    public SensorReading createSensorReading(Long sensorId, Double temperatureValue, Double humidityValue) {
        return sensorService.createSensorReading(sensorId,temperatureValue,humidityValue);
    }


    @PostMapping("/createFakeSensorReading")
    public SensorReading createFakeSensorReading(Long sensorId) {
       return createSensorReading(sensorId, generateRandomTemperatureValue(),generateRandomHumidityValue());
    }
    @GetMapping("/getAllSensors")
    public List<Sensor> getAllSensors(){
        return sensorService.getAllSensors();
    }
    @GetMapping("/getSensor")
    public Sensor getSensor(Long sensorId){
        return sensorService.getSensorWithReadingsById(sensorId);
    }
    private Double generateRandomTemperatureValue() {
        double minValue = 15.0f;
        double maxValue = 35.0f;
        return Double.valueOf(ThreadLocalRandom.current().nextFloat() * (maxValue - minValue) + minValue);
    }

    private Double generateRandomHumidityValue() {
        double minValue = 0.0f;
        double maxValue = 100.0f;
        return Double.valueOf(ThreadLocalRandom.current().nextFloat() * (maxValue - minValue) + minValue);
    }

}
