package com.neanex.exercise.controller;

import com.neanex.exercise.model.Building;
import com.neanex.exercise.model.SensorReadingSummary;
import com.neanex.exercise.service.BuildingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/buildings")
public class BuildingController {

    private final BuildingService buildingService;
    @Autowired
    public BuildingController(BuildingService buildingService) {
        this.buildingService = buildingService;
    }


    @GetMapping("/getAllBuildings")
    public List<Building> getAllBuildings() {
        return buildingService.findAllBuildings();
    }

    /**
     * Getting maximal and minimal values of humidity and temperature
     * @param buildingIds
     * @param startDate
     * @param endDate
     * @return
     */
    @GetMapping("/getBuildingsSensorReadingSummary")
    public SensorReadingSummary getBuildingsSensorReadingSummary(@RequestParam("buildingIds") List<Long> buildingIds,
                                                                @RequestParam(value = "startDate", required = false) String startDate,
                                                                @RequestParam(value = "endDate", required = false) String endDate) {
        return buildingService.getBuildingSensorReadingSummary(buildingIds,startDate,endDate);
    }
    /**
     * Calculates the average temperature of the specified type (temperature or humidity) in the given buildings.
     * @param buildingIds      the list of building IDs
     * @return the average reading value
     */
    @GetMapping("/getAverageTemperature")
    public Double getAverageTemperature(@RequestParam("buildingIds") List<Long> buildingIds,
                                               @RequestParam(value = "startDate", required = false) String startDate,
                                               @RequestParam(value = "endDate", required = false) String endDate) {
        return buildingService.getAverageTemperature(buildingIds,startDate,endDate);
    }
    /**
     * Calculates the average humidity of the specified type (temperature or humidity) in the given buildings.
     * @param buildingIds      the list of building IDs
     * @return the average reading value
     */
    @GetMapping("/getAverageHumidity")
    public Double getAverageHumidity(@RequestParam("buildingIds") List<Long> buildingIds,
                                            @RequestParam(value = "startDate", required = false) String startDate,
                                            @RequestParam(value = "endDate", required = false) String endDate) {
        return buildingService.getAverageHumidity(buildingIds,startDate,endDate);
    }

}
