package com.neanex.exercise.controller;


import com.neanex.exercise.model.Room;
import com.neanex.exercise.model.SensorReadingSummary;
import com.neanex.exercise.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RequestMapping("/rooms")
public class RoomController {


    private final RoomService roomService;

    @Autowired
    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }

    /**
     * Calculates the average temperature of the specified type (temperature or humidity) in the given rooms.
     * @param roomIds      the list of room IDs
     * @return the average reading value
     */
    @GetMapping("/getAverageTemperature")
    public Double getAverageTemperature(@RequestParam("roomIds") List<Long> roomIds,
                                           @RequestParam(value = "startDate", required = false) String startDate,
                                           @RequestParam(value = "endDate", required = false) String endDate) {
        return roomService.getAverageTemperature(roomIds,startDate,endDate);
    }
    /**
     * Calculates the average humidity of the specified type (temperature or humidity) in the given rooms.
     * @param roomIds      the list of room IDs
     * @return the average reading value
     */
    @GetMapping("/getAverageHumidity")
    public Double getAverageHumidity(@RequestParam("roomIds") List<Long> roomIds,
                                     @RequestParam(value = "startDate", required = false) String startDate,
                                     @RequestParam(value = "endDate", required = false) String endDate) {
        return roomService.getAverageHumidity(roomIds,startDate,endDate);
    }
    /**
     * Retrieves the reading value in rooms based on the provided parameters.
     *
     * @param roomIds     the list of room IDs to consider
     * @param readingType the type of reading (temperature or humidity)
     * @param valueType   the type of value to retrieve (max or min)
     * @param startDate   (optional) the start date for filtering the sensor readings
     * @param endDate     (optional) the end date for filtering the sensor readings
     * @return the reading value in rooms based on the specified reading type and value type,
     *         or null if the reading type or value type is invalid
     */
    @GetMapping("/getReadingValueInRooms")
    public Double getReadingValueInRooms(@RequestParam("roomIds") List<Long> roomIds,
                                         @RequestParam("readingType") String readingType,
                                         @RequestParam("valueType") String valueType,
                                         @RequestParam(value = "startDate", required = false) String startDate,
                                         @RequestParam(value = "endDate", required = false) String endDate) {
        SensorReadingSummary summary = roomService.getSensorReadingSummary(roomIds, startDate, endDate);

        if (readingType.equalsIgnoreCase("temperature")) {
            if (valueType.equalsIgnoreCase("max")) {
                return summary.getMaxTemperature();
            } else if (valueType.equalsIgnoreCase("min")) {
                return summary.getMinTemperature();
            }
        } else if (readingType.equalsIgnoreCase("humidity")) {
            if (valueType.equalsIgnoreCase("max")) {
                return summary.getMaxHumidity();
            } else if (valueType.equalsIgnoreCase("min")) {
                return summary.getMinHumidity();
            }
        }
        return null;
    }

    @GetMapping("/getAllRoomIds")
    public List<Long> getAllRoomIds() {
        return roomService.getAllRoomIds();
    }

    @GetMapping("/getRoomWithSensorsAndReadingsById")
    public Room getRoomWithSensorsAndReadingsById(@RequestParam("roomId") Long roomId) {
        return roomService.getRoomWithSensorsAndReadingsById(roomId);
    }

}
