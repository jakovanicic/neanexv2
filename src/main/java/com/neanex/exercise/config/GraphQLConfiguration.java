package com.neanex.exercise.config;

import com.neanex.exercise.query.TemperatureQuery;
import graphql.GraphQL;
import graphql.Scalars;
import graphql.schema.GraphQLArgument;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLList;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

import static graphql.scalars.ExtendedScalars.GraphQLLong;

@Configuration
public class GraphQLConfiguration {

    private final TemperatureQuery temperatureQuery;

    public GraphQLConfiguration(TemperatureQuery temperatureQuery) {
        this.temperatureQuery = temperatureQuery;
    }

    @Bean
    public GraphQL graphQL() {
        GraphQLSchema schema = buildSchema();
        return GraphQL.newGraphQL(schema).build();
    }

    private GraphQLSchema buildSchema() {
        return GraphQLSchema.newSchema()
                .query(buildQueryType())
                .build();
    }

    private GraphQLObjectType buildQueryType() {
        return GraphQLObjectType.newObject()
                .name("Query")
                .field(buildAverageTemperatureField())
                .build();
    }

    private GraphQLFieldDefinition buildAverageTemperatureField() {
        return GraphQLFieldDefinition.newFieldDefinition()
                .name("getAverageTemperature")
                .type(Scalars.GraphQLFloat)
                .argument(buildArguments())
                .dataFetcher(env -> {
                    List<Long> buildingIds = env.getArgument("buildingIds");
                    String startDate = env.getArgument("startDate");
                    String endDate = env.getArgument("endDate");
                    return temperatureQuery.getAverageTemperature(buildingIds, startDate, endDate);
                })
                .build();
    }


    private GraphQLArgument buildArguments() {
        return GraphQLArgument.newArgument()
                .name("buildingIds")
                .type(GraphQLList.list(GraphQLLong))
                .build();
    }

}
